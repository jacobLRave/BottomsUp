package com.example.BottomssUp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.bottomsup.databinding.ItemDrinksBinding
import com.example.bottomsup.model.Rsponse.CategoryDTO
import com.example.bottomsup.model.Rsponse.CategoryDrinksDTO
import com.google.android.material.card.MaterialCardView

class ListAdapter(val nav: (String) -> Unit) : RecyclerView.Adapter<ListAdapter.InputViewHolder>() {

    private var category = mutableListOf<CategoryDrinksDTO.Drink>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InputViewHolder {
        val binding = ItemDrinksBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return InputViewHolder(binding)
    }

    override fun onBindViewHolder(holder: InputViewHolder, position: Int) {
        val category = category[position]
        holder.loadCategory(category)
        holder.navigateClick().setOnClickListener() {
            nav(category.idDrink)
        }
    }

    override fun getItemCount(): Int {
        return category.size
    }

    fun addCategory(category: List<CategoryDrinksDTO.Drink>) {
        this.category = category.toMutableList()
        notifyDataSetChanged()
    }

    class InputViewHolder(
        private val binding: ItemDrinksBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadCategory(category: CategoryDrinksDTO.Drink) {
            binding.tvStrDrink.text = category.strDrink

            Glide.with(binding.siDrink).load(category.strDrinkThumb).into(binding.siDrink)
        }

        fun navigateClick(): MaterialCardView {
            return binding.root
        }

    }

}