package com.example.BottomssUp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.bottomsup.databinding.ItemCardBinding
import com.example.bottomsup.databinding.ItemDetailsBinding
import com.example.bottomsup.model.Rsponse.DrinkDetailsDTO
import com.google.android.material.card.MaterialCardView

class DetailsAdapter(val nav: (String) -> Unit) :
    RecyclerView.Adapter<DetailsAdapter.InputViewHolder>() {

    private var category = mutableListOf<DrinkDetailsDTO.Drink>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InputViewHolder {
        val binding = ItemCardBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return InputViewHolder(binding)
    }

    override fun onBindViewHolder(holder: InputViewHolder, position: Int) {
        val category = category[position]
        holder.loadCategory(category)
        holder.navigateClick().setOnClickListener() {
            nav(category.idDrink)
        }
    }

    override fun getItemCount(): Int {
        return category.size
    }

    fun addCategory(category: List<DrinkDetailsDTO.Drink>) {
        this.category = category.toMutableList()
        notifyDataSetChanged()
    }

    class InputViewHolder(
        private val binding: ItemCardBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadCategory(details: DrinkDetailsDTO.Drink) {
            with(binding) {
                var ingredients = buildIngredients(details)


                tvStrDrink.text = details.strDrink
                tvStrCategory.text = details.strCategory
                tvStrAlcoholic.text = details.strAlcoholic
                tvStrGlass.text = details.strGlass
                tvStrIngredient.text = ingredients
                tvStrInstructions.text = details.strInstructions

            }

            Glide.with(binding.siDrink).load(details.strDrinkThumb).into(binding.siDrink)
        }

        fun navigateClick(): MaterialCardView {
            return binding.root
        }

        private fun buildIngredients(details: DrinkDetailsDTO.Drink): String {
            var ingredients =
                "${checkStringNull(details.strIngredient1)}${checkStringNull(details.strIngredient2)}${
                    checkStringNull(details.strIngredient3)
                }${checkStringNull(details.strIngredient4)}".dropLast(2)


            return ingredients
        }

        private fun checkStringNull(ingredient: String?): String {
          return  if (ingredient != null) "$ingredient, " else ""
        }

    }

}