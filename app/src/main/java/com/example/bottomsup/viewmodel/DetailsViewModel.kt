package com.example.bottomsup.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.bottomsup.model.BottomsUpRepo
import com.example.bottomsup.model.Rsponse.CategoryDTO
import com.example.bottomsup.model.Rsponse.CategoryDrinksDTO
import com.example.bottomsup.model.Rsponse.DrinkDetailsDTO
import com.example.bottomsup.view.category.CategoryState
import kotlinx.coroutines.launch

class DetailsViewModel : ViewModel() {

    private val repo by lazy { BottomsUpRepo }

    private val _state = MutableLiveData(CategoryState<DrinkDetailsDTO.Drink>(isLoading = true))
    val state: LiveData<CategoryState<DrinkDetailsDTO.Drink>> get() = _state
    fun getDetails(type: String) {
        viewModelScope.launch {
            val drinksDTO = repo.getDetails(type)
            _state.value = CategoryState(categories = drinksDTO.drinks)
        }
    }


}