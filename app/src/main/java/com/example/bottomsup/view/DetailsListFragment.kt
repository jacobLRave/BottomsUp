package com.example.bottomsup.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.example.BottomssUp.adapter.DetailsAdapter
import com.example.bottomsup.databinding.FragmentDetailsBinding
import com.example.bottomsup.viewmodel.DetailsViewModel

class DetailsListFragment : Fragment() {
    private var _binding: FragmentDetailsBinding? = null
    private val binding get() = _binding!!
    private val args by navArgs<DetailsListFragmentArgs>()

    private val detailsViewModel by viewModels<DetailsViewModel>()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentDetailsBinding.inflate(inflater, container, false).also {
        _binding = it

    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        detailsViewModel.getDetails(args.id)
        detailsViewModel.state.observe(viewLifecycleOwner) { state ->
            binding.rvList.apply {
                adapter = DetailsAdapter(::navigate).apply {
                    addCategory(state.categories)
                }
            }
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    fun navigate(list: String) {

    }

}